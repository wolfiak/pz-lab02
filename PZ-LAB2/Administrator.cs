﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
    class Administrator : Pracownik
    {
        public int liczbaSerwerow { get; set; }
        public Administrator(int ID, string imie, string nazwisko, bool staz, double pensja, int liczbaSerwerow) : base(ID,imie,nazwisko,staz,pensja)
        {
            this.liczbaSerwerow = liczbaSerwerow;
        }

        public override string ToString()
        {
            return base.ToString() + $" Liczba serwerow: {this.liczbaSerwerow}";
        }
        public override void wprowadz()
        {
            base.wprowadz();
            Console.Write("Podaj liczbe serwerow: ");
            this.liczbaSerwerow=Convert.ToInt32(Console.ReadLine());

        }
        public override void zwieksz(double pensja)
        {
            base.zwieksz(pensja * liczbaSerwerow);
        }

        public Administrator()
        {

        }
    }
}
