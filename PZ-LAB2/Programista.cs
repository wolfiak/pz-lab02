﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
    class Programista : Pracownik
    {
        private int LiczbaTechnologi;

        public int MyProperty
        {
            get { return LiczbaTechnologi; }
            set { LiczbaTechnologi = value; }
        }

        public Programista(int ID, string imie, string nazwisko, bool staz, double pensja) :base(ID,imie,nazwisko,staz,pensja)
        {
            this.LiczbaTechnologi = LiczbaTechnologi;
        }

        public override string ToString()
        {
            return base.ToString() + $" Liczba technologii: {this.LiczbaTechnologi}"  ;
        }
        public override void wprowadz()
        {
            base.wprowadz();
            Console.Write("Podaj liczbe technologii: ");
            this.LiczbaTechnologi = Convert.ToInt32(Console.ReadLine());
        }
        public override void zwieksz(double pensja)
        {
            base.zwieksz(pensja * LiczbaTechnologi);
        }
        public Programista()
        {

        }

    }
}
