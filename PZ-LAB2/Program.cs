﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
    class Program
    {
        static void Main(string[] args)
        {
            Managment.oninit();
            Pracownik lol=new Mendadzer(1,"darek","zegarek",2000,true,2);
           // Pracownik lol = new Mendadzer();
          //  lol.wprowadz();
           // Console.WriteLine(lol.ToString());

            Pracownik p2 = new Zdalny();
           // Console.WriteLine(p2.ToString());
           // Console.WriteLine("Wypisywanie z Managment");
            Managment.addPracownik(lol);

           
            bool dzialanie = true;
            while (dzialanie)
            {
                Console.WriteLine("Wybierz opcje");
                Console.WriteLine("1.Wyswietlanie wszystkich pracownikow");
                Console.WriteLine("2.Wyswietlanie po kategorii pracownika");
                Console.WriteLine("3.Zwieksz pensje pracownika");
                Console.WriteLine("4.Zwieksz pensje dzialu");
                Console.WriteLine("5.Zwieksz pensje WSZYSTKIM");
                Console.WriteLine("6.Statystyki");
                Console.WriteLine("7.Dodaj pracowanika");
                Console.WriteLine("8.Usun pracownika");
                Console.WriteLine("0.Koncz wasc");
                Console.Write("Wybor: ");
                int wybor = Convert.ToInt32(Console.ReadLine());
                switch (wybor)
                {
                    case 1:
                        {
                            Managment.wypisywanieWszystkich();
                            break;
                        }
                    case 2:
                        {
                            Managment.wyswietlanieKategorii(new ZlecenieWyswietlId(),false);
                            break;
                        }
                    case 3:
                        {
                            Managment.zwiekszPensjeJednego();
                            break;
                        }
                    case 4:
                        {
                            Managment.zwiekszPensjeKategorii();
                            break;
                        }
                    case 5:
                        {
                            Console.Write("O ile chcesz zwiekszyc pensje: ");
                            double pensja = Convert.ToDouble(Console.ReadLine());
                            Managment.zwiekszPensjeWszystkim(pensja);
                            break;
                        }
                    case 6:
                        {
                            Console.WriteLine("1.Statystyki wszystkich");
                            Console.WriteLine("2.Statystyki dzialu");
                            int wybor2 = Convert.ToInt32(Console.ReadLine());
                            switch (wybor2)
                            {
                                case 1:
                                    {
                                        Statystyki.statytykiWszystkich();
                                        break;
                                    }
                                case 2:
                                    {
                                        Statystyki.statystykaDzialu();
                                        break;
                                    }
                            }
                            break;
                        }
                    case 7:
                        {
                            Managment.dodajPracownika();
                            break;
                        }
                    case 8:
                        {
                            Managment.usuwaniePracownika();
                            break;
                        }
                    case 0:
                        {
                            Console.WriteLine("Koniec dzialania programu");
                            dzialanie = false;
                            break;
                        }
                }

            }

            Console.ReadLine();
        }
    }
}
