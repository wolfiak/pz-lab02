﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
    static class Statystyki
    {
        public static void statytykiWszystkich()
        {
            double pensja = 0;
            Managment.lista.ForEach(k =>
            {
                pensja += k.pensja;
            });

            Console.WriteLine($"Koszt pensji w miesiacu: {pensja}");
            Console.WriteLine($"Koszt calego roku: {pensja*12}");
        }
        public static void statystykaDzialu()
        {
            Managment.wyswietlanieKategorii(new ZlecenieStatystykiDzialu(),false);
        }
    }
}
