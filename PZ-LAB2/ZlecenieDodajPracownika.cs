﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
    class ZlecenieDodajPracownika : IZlecenie
    {
        public void wykonaj()
        {
            Console.WriteLine("TU DODWANIE");
            Kategoria k=ListaKategorii.getKategoria(Managment.wyboro);
            var paracownik = Activator.CreateInstance(k.Typ.GetType());
            Pracownik p =(Pracownik) paracownik;
            p.wprowadz();
            Console.WriteLine("Utworzylem: "+paracownik.ToString() );
            Managment.lista.Add(p);
        }
    }
}
