﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
     abstract class Pracownik
    {
        public int ID { get; set; }
        public string imie { get; set; }
        public string nazwisko { get; set; }
        public bool staz { get; set; }
        public double pensja { get; set; }
        public Pracownik()
        {
            this.ID = 0;
            this.imie = "dupa";
            this.nazwisko="";
            staz = false;
            pensja = 0;
        }
        public Pracownik(int ID, string imie, string nazwisko, bool staz, double pensja)
        {
            this.ID = ID;
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.staz = staz;
            this.pensja = pensja;
        }

        public override string ToString()
        {
            return String.Format($"Imie: {this.imie}  Nazwisko: {this.nazwisko} Staz: {this.staz.ToString()} Pensja: {this.pensja} ");
        }

        public virtual void wprowadz()
        {
            Console.WriteLine("Podaj imie:");
            this.imie = Console.ReadLine();
            Console.WriteLine("Podaj nazwisko:");
            this.nazwisko = Console.ReadLine();
            Console.WriteLine("Podaj Staz:");
            this.staz = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine("Podaj Pensja:");
            this.pensja = Convert.ToDouble(Console.ReadLine());
        }

        public virtual void zwieksz(double pensja)
        {
            this.pensja += pensja;
        }
    }
}
