﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
    class Zdalny : Programista
    {
        public int odleglosc { get; set; }

        public Zdalny(int ID, string imie, string nazwisko, bool staz,double pensja, int odleglosc) : base(ID,imie,nazwisko,staz,pensja)
        {
            this.odleglosc = odleglosc;
        }

        public override void wprowadz()
        {
            base.wprowadz();
            Console.Write("Podaj odleglosc");
            this.odleglosc= Convert.ToInt32(Console.ReadLine());
        }
        public override void zwieksz(double pensja)
        {
            base.zwieksz(pensja * odleglosc);
        }
        public Zdalny() 
        {
         
        }
    }
}
