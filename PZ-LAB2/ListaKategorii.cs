﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
    static class ListaKategorii
    {
       private static List<Kategoria> lista = new List<Kategoria>();
        public static List<string> kategorie { get; set; } = new List<string>();

        public static Kategoria getKategoria(string s)
        {
            return lista.Find(p => p.Nazwa == s);
        }
        public static Kategoria getKategoria(int s)
        {
            Kategoria ka=null;
            int licznik = 0;
            lista.ForEach(k =>
            {

                if(s == licznik)
                {
                     ka=k;
                }
                licznik++;
            });
            return ka;
        }
        public static void usunPracowanika()
        {
            //foreach(Kategoria k in lista)
            //{
            //    Console.WriteLine($"l.getype: {lista.GetType().BaseType.Name} l.nazwa {k.Nazwa}");
            //}
            lista.RemoveAll(l => l.GetType().BaseType.Name == l.Nazwa);
        }
        public static void addKategoria(string nazwa,Pracownik p)
        {
            lista.Add(new Kategoria(p, nazwa));
        }

        public static Pracownik szukajPoId(int id)
        {
            Pracownik pra=null;
            int licznik = 0;
            lista.ForEach(k =>
            {
                if (licznik == id)
                {
                    pra= k.Typ;
                }
                licznik++;
            });
            return pra;
        }
    }
}
