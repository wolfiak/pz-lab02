﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
    static class Managment
    {
       public static List<Pracownik> lista { get; set; } = new List<Pracownik>();
        public static int wyboro { get; set; }

        //public Managment()
        //{
        //    lista = new List<Pracownik>();

        //}
        public static Pracownik szukajPoPracowniku(Pracownik p)
        {
            Pracownik pra=null;
            foreach(Pracownik i in lista)
            {
              //  Console.WriteLine($"p.gettype: {p.GetType()} == i.gettype: {i.GetType()}");
                if (p.GetType() == i.GetType())
                {
                    pra= i;
                }
            }
            return pra;
        }
        public static void wypisywanieWszystkich()
        {
            int licznik = 0;
            foreach (Pracownik p in lista)
            {
                Console.WriteLine(licznik+". "+p.ToString());
                licznik++;
            }
        }
        public static void addPracownik(Pracownik p)
        {
            lista.Add(p);
        }
        public static void oninit()
        {
            var typ = typeof(Pracownik);
            
            var typy = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typ.IsAssignableFrom(p));



            typy.ToList().ForEach(x => {

                if (x.BaseType != typeof(Object))
                {


                    ListaKategorii.kategorie.Add(x.Name);
                   

                    if (x.BaseType == typeof(Pracownik))
                    {
                        // Console.WriteLine($"{x}");
                        Pracownik p = (Pracownik)Activator.CreateInstance(x);
                        ListaKategorii.addKategoria(x.Name, p);
                    }
                }
            });
            }
        public static void wyswietlanieKategorii(IZlecenie z,bool tryb)
        {

            //var typ = typeof(Pracownik);
            //var typy = AppDomain.CurrentDomain.GetAssemblies()
            //    .SelectMany(s => s.GetTypes())
            //    .Where(p => typ.IsAssignableFrom(p));

            //// Console.WriteLine($"typy {typy.GetType()}");
            //int licznik = 0;
            //typy.ToList().ForEach(x => {
            //    // lista.Add(x.Name x.BaseType);

            //   // Console.WriteLine($"Zwykly x: {x}");
            //  //  Console.WriteLine($"Base: {x.BaseType}");
            //    if (x.BaseType != typeof(Object))
            //    {
            //        ListaKategorii.kategorie.Add(x.Name);
            //        Console.WriteLine($"{licznik}. {x.Name}");

            //        if (x.BaseType == typeof(Pracownik))
            //        {
            //            // Console.WriteLine($"{x}");
            //            Pracownik p = (Pracownik)Activator.CreateInstance(x);
            //            ListaKategorii.addKategoria(x.Name, p);
            //        }
            //        licznik++;
            //        //else
            //{
            //    Console.WriteLine($"Nie jest {x}");

            //}


            //   p
            //Console.WriteLine($"{p.ToString()}");
            //        //  ListaKategorii.addKategoria(x.Name,p);
            //    }
            //});

            //ListaKategorii.usunPracowanika();
            int licznik=0;
            ListaKategorii.kategorie.ForEach(i =>
            {
                Console.WriteLine($"{licznik}. {i} ");
                licznik++;
            });
            Console.Write("Wybierz kategorie:");
            int wybor = Convert.ToInt32(Console.ReadLine());
            if (szukajPoPracowniku(ListaKategorii.szukajPoId(wybor)) == null && tryb==false)
            {
                Console.WriteLine("Ten dzial jest pusty");
            }else
            {
                wyboro = wybor;
                z.wykonaj();
              //Console.WriteLine(szukajPoPracowniku(ListaKategorii.szukajPoId(wybor)).ToString());
            }

        //    Console.WriteLine(szukajPoPracowniku(ListaKategorii.szukajPoId(wybor)).ToString() == null ? "Nie ma pracownikow w tej kategorii" : szukajPoPracowniku(ListaKategorii.szukajPoId(wybor)).ToString());
            //foreach ()
            //{

            //}

        }
        public static void zwiekszPensje(Pracownik p,double plus)
        {
            lista.Where(i => i.ID == p.ID).First().pensja += plus;
        }
      

        public static void zwiekszPensjeJednego()
        {
            Console.WriteLine("Wybierz pracownika do zwiekszenia pensji:");
            int licznik=0;
            lista.ForEach(i =>
            {
                Console.WriteLine($"{licznik}. {i.ToString()}");
            });
            Console.Write("Wybor: ");
            int wybor = Convert.ToInt32(Console.ReadLine());
            Console.Write("O ile podniesc pensje: ");
            double pensja = Convert.ToDouble(Console.ReadLine());
            zwiekszPensje(lista.ElementAt(wybor), pensja);
        }
        public static void zwiekszPensjeKategorii()
        {
            Console.WriteLine("Wybierz kategorie");
            int licznik = 0;
            ListaKategorii.kategorie.ForEach(i =>
            {
                Console.WriteLine($"{licznik}. {i}");
                licznik++;
            });
            Console.Write("Wybor:");
            int wybor = Convert.ToInt32(Console.ReadLine());
            Console.Write("Podaj pensje do podwyzki: ");
            double pensja = Convert.ToDouble(Console.ReadLine());
            zwiekszDzial(ListaKategorii.getKategoria(ListaKategorii.kategorie.ElementAt(wybor)), pensja);
        }
        public static void zwiekszDzial(Kategoria k, double pensja)
        {
            lista.ForEach(i =>
            {
                if(k.Typ.GetType() == i.GetType())
                {
                    i.pensja += pensja;
                }
            });
        }
        public static void zwiekszPensjeWszystkim(double pensja)
        {

            lista.ForEach(k => k.zwieksz(pensja));
        }
        public static void dodajPracownika()
        {
            wyswietlanieKategorii(new ZlecenieDodajPracownika(),true);
        }
        public static void usuwaniePracownika()
        {
            
            wypisywanieWszystkich();
            Console.Write("Ktorego dziada usunac: ");
            int n = Convert.ToInt32(Console.ReadLine());
            int licznik = 0;
            Pracownik p=null;
            lista.ForEach(k =>
            {
                if (licznik == n)
                {
                    p = k;
                }

            });
            lista.Remove(p);
        }
    }
}
