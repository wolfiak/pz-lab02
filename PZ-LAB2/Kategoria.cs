﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
    class Kategoria
    {
        public Pracownik Typ { get; set; }
        public string Nazwa { get; set; }

        public Kategoria(Pracownik Typ, string Nazwa)
        {
            this.Typ = Typ;
            this.Nazwa = Nazwa;
        }
    }
}
