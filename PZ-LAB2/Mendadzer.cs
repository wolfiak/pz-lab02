﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
    class Mendadzer : Pracownik
    {
        public int liczbaP { get; set; }

        public Mendadzer(int ID, string imie, string nazwisko, double pensja, bool staz, int liczbaP) :base(ID,imie,nazwisko,staz,pensja)
        {
            this.liczbaP = liczbaP;
        }
        public override string ToString()
        {
            return base.ToString() + $"Liczba projektow: {this.liczbaP}";
        }
        public override void wprowadz()
        {
            base.wprowadz();
            Console.WriteLine("Wprwoadz liczbe projektow");
            this.liczbaP = Convert.ToInt32(Console.ReadLine());
        }
        public override void zwieksz(double pensja)
        {
            base.zwieksz(pensja * liczbaP);
        }
        public Mendadzer() 
        {

        }
    }
}
