﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB2
{
    class ZlecenieStatystykiDzialu : IZlecenie
    {
        public void wykonaj()
        {
            double pensja = 0;
            Managment.lista.ForEach(i =>
            {
                Console.WriteLine($"{i.GetType()} == {ListaKategorii.getKategoria(ListaKategorii.kategorie.ElementAt(Managment.wyboro)).Typ.GetType()}");
                if (i.GetType() == ListaKategorii.getKategoria(ListaKategorii.kategorie.ElementAt(Managment.wyboro)).Typ.GetType()) 
                {
                    Console.WriteLine("Zaszlo");
                    pensja += i.pensja;
                }
            });
            Console.WriteLine($"Wydatki na Pensje miesieczne w dziele {ListaKategorii.kategorie.ElementAt(Managment.wyboro)}: {pensja}");
            Console.WriteLine($"Wydatki na Pensje roczne w dziele {ListaKategorii.kategorie.ElementAt(Managment.wyboro)}: {pensja*12}");
        }
    }
}
